package com.study.dao.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.study.dao.IPedidoDao;
import com.study.model.ItemPedido;
import com.study.model.Pedido;

public class PedidoDao extends AbstractDao implements IPedidoDao{

	public PedidoDao(Connection conn) {
		super(conn);
	}

	public void insert(Pedido model) throws Exception {
		final String SQL = "insert into pedidos (codigo_cliente,data_pedido) "
				         + "values(?,?)";
		PreparedStatement ps = conn.prepareStatement(SQL);
		ps.setInt(1,model.getCliente().getCodigo());
		ps.setDate(2,model.getDataPedido());
		ps.execute();
		ps.close();
	}

	public void update(Pedido model) throws Exception {
		final String SQL = "update pedidos set "
				         + "codigo_cliente=?,data_pedido=? "
				         + "where numero_pedido=?";
		PreparedStatement ps = conn.prepareStatement(SQL);
		ps.setInt(1,model.getCliente().getCodigo());
		ps.setDate(2, model.getDataPedido());
		ps.setInt(3, model.getNumeroPedido());
		ps.execute();
		ps.close();
	}

	public void delete(Object id) throws Exception {
		final String SQL = "delete from pedidos where numero_pedido = ?";
		PreparedStatement ps = conn.prepareStatement(SQL);
		ps.setInt(1,(Integer) id);
		ps.execute();
		ps.close();
	}

	public List<Pedido> listAll() throws Exception {
		final String SQL = "select * from pedidos";
		List<Pedido> result = new ArrayList<Pedido>();
		PreparedStatement ps = conn.prepareStatement(SQL);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			result.add(buildModel(rs));
		}
		return result;
	}

	public Pedido get(Object id) throws Exception {
		final String SQL = "select * from pedidos where numero_pedido=?";
		Pedido result = new Pedido();
		PreparedStatement ps = conn.prepareStatement(SQL);
		ps.setInt(1, (Integer) id);
		ResultSet rs = ps.executeQuery();
		if(rs.next()){
			result = buildModel(rs);
		}
		return result;
	}

	public List<Pedido> findByDataPedido(Date dataPedido) throws Exception {
		final String SQL = "select * from pedidos where data_pedido=?";
		List<Pedido> result = new ArrayList<Pedido>();
		PreparedStatement ps = conn.prepareStatement(SQL);
		ps.setDate(1, dataPedido);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			result.add(buildModel(rs));
		}
		return result;
	}
	
	private Pedido buildModel(ResultSet rs) throws Exception {
		Pedido result = new Pedido();
		result.setNumeroPedido(rs.getInt("numero_pedido"));
		result.setCliente(new ClienteDao(conn).get((rs.getInt("codigo_cliente"))));
		List<ItemPedido> list = new ItemPedidoDao(conn).findByPedido(result);
		result.setItemPedidoList(list);
		result.setDataPedido(rs.getDate("data_pedido"));
		return result;
	}

}
