package com.study.jdbc;

import java.sql.Connection;

import javax.naming.InitialContext;
import javax.sql.DataSource;

/*
 * Pool de conex�o com o banco de dados
 */
public class ConnectionFactory {
	public Connection getConnection() throws Exception{
			InitialContext contexto = new InitialContext();
			DataSource ds = (DataSource)contexto.lookup("java:comp/env/jdbc/studyweb");
			return ds.getConnection();
		}
}

