package com.study.rest;

import java.sql.Connection;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.study.dao.IProdutoDao;
import com.study.jdbc.ConnectionFactory;
import com.study.dao.jdbc.ProdutoDao;
import com.study.model.Produto;

@Path("/produto")
public class ProdutoResource {

	//select by id
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Produto get(@PathParam("id") String id) {
		try {
			Connection conn = new ConnectionFactory().getConnection();
			IProdutoDao dao = new ProdutoDao(conn);
			Produto produto = dao.get(id);
			return produto;
		} catch (Exception e) {
			return null;
		}
	}
	
	//insert
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.TEXT_PLAIN)
		public String post(Produto produto) {
			try {
				Connection conn = new ConnectionFactory().getConnection();
				IProdutoDao dao = new ProdutoDao(conn);
				dao.insert(produto);
			return "{\"msg\": \"Produto inserido com sucesso!\"}";
			} catch (Exception e) {
				return "{\"msg\": \"Erro ao inserir o produto!\"}";
			}
		}
		
		//update
		@PUT
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public String put(Produto produto){
			try {
				Connection conn = new ConnectionFactory().getConnection();
				IProdutoDao dao = new ProdutoDao(conn);
				dao.update(produto);
				return "{\"msg\": \"Produto atualizado com sucesso!\"}";
			} catch (Exception e) {
				return "{\"msg\": \"Erro a atualizar o produto!\"}";
			}
		}
		
		//delete
		@DELETE
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public String delete(Produto produto){
			try {
				Connection conn = new ConnectionFactory().getConnection();
				IProdutoDao dao = new ProdutoDao(conn);
				dao.delete(produto.getCodigo());
				return "{\"msg\": \"Produto exclu�do com sucesso!\"}";
			} catch (Exception e) {
				return "{\"msg\": \"Houve um problema ao excluir o produto!\"}";
			}
		}
		
		//select all
		@GET
		@Path("/all")
		@Produces(MediaType.APPLICATION_JSON)
		public List<Produto> list() {
			try {
				List<Produto> result = null;
				Connection conn = new ConnectionFactory().getConnection();
				IProdutoDao dao = new ProdutoDao(conn);
				result = dao.listAll();
				return result;
			} catch (Exception e) {
				return null;
			}
		}
	
}
