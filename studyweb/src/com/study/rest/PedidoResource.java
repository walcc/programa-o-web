package com.study.rest;

import java.sql.Connection;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.study.dao.IPedidoDao;
import com.study.jdbc.ConnectionFactory;
import com.study.dao.jdbc.PedidoDao;
import com.study.model.Pedido;

@Path("/pedido")
public class PedidoResource {

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Pedido get(@PathParam("id") int id){
		try {
			Connection conn = new ConnectionFactory().getConnection();
			IPedidoDao dao = new PedidoDao(conn);
			Pedido pedido = dao.get(id);
			return pedido;
		} catch (Exception e) {
			return null;
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String post(Pedido pedido){
		try {
			Connection conn = new ConnectionFactory().getConnection();
			IPedidoDao dao = new PedidoDao(conn);
			dao.insert(pedido);
			return "{\"msg\": \"Pedido inserido com sucesso!\"}";
		} catch (Exception e) {
			return "{\"msg\": \"Erro ao inserir o pedido!\"}";
		}
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String put(Pedido pedido){
		try {
			Connection conn = new ConnectionFactory().getConnection();
			IPedidoDao dao = new PedidoDao(conn);
			dao.update(pedido);
			return "{\"msg\": \"Pedido atualizado com sucesso!\"}";
		} catch (Exception e) {
			return null;
		}
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String delete(Pedido pedido){
		try {
			Connection conn = new ConnectionFactory().getConnection();
			IPedidoDao dao = new PedidoDao(conn);
			dao.delete(pedido.getNumeroPedido());
			return "{\"msg\": \"Pedido exclu�do com sucesso!\"}";
		} catch (Exception e) {
			return "{\"msg\": \"Houve um problema ao excluir o pedido!\"}";
		}
	}
	
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Pedido> list(){
		try {
			Connection conn = new ConnectionFactory().getConnection();
			IPedidoDao dao = new PedidoDao(conn);
			return dao.listAll();
		} catch (Exception e) {
			return null;
		}
	}
}
