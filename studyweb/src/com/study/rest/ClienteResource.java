package com.study.rest;

import java.sql.Connection;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.study.dao.IClienteDao;
import com.study.dao.jdbc.ClienteDao;
import com.study.jdbc.ConnectionFactory;
import com.study.model.Cliente;

@Path("/cliente")
public class ClienteResource {
	
	
	//select by id
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Cliente get(@PathParam("id") int id) {
		try {
			Connection conn =   new ConnectionFactory().getConnection();
			IClienteDao dao = new ClienteDao(conn);
			return dao.get(id);
		} catch (Exception e) {
			return null;
		}
	}
	
	//insert
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String post(Cliente cliente){
		try {
			Connection conn = new ConnectionFactory().getConnection();
			IClienteDao dao = new ClienteDao(conn);
			dao.insert(cliente);
			return "{\"msg\": \"Cliente inserido com sucesso!\"}";
		} catch (Exception e) {
			return "{\"msg\": \"Erro ao inserir o cliente!\"}";
		}
	}
	
	//update
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String put(Cliente cliente){
		try {
			Connection conn = new ConnectionFactory().getConnection();
			IClienteDao dao = new ClienteDao(conn);
			dao.update(cliente);
			return "{\"msg\": \"Cliente atualizado com sucesso!\"}";
		} catch (Exception e) {
			return "{\"msg\": \"Erro ao atualizar o cliente!\"}";
		}
	}
	
	//delete
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String delete(Cliente cliente){
		try {
			Connection conn = new ConnectionFactory().getConnection();
			IClienteDao dao = new ClienteDao(conn);
			dao.delete(cliente.getCodigo());
			return "{\"msg\": \"Cliente deletado com sucesso!\"}";
		} catch (Exception e) {
			return "{\"msg\": \"Erro ao deletar o cliente!\"}";
		}
	}
	
	//select all
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Cliente> list() {
		try {
			List<Cliente> result = null;
			Connection conn = new ConnectionFactory().getConnection();
			IClienteDao dao = new ClienteDao(conn);
			result = dao.listAll();
			return result;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}
}
