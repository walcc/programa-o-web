package com.study.rest;

import java.sql.Connection;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.study.dao.IItemPedidoDao;
import com.study.dao.IPedidoDao;
import com.study.dao.IProdutoDao;
import com.study.jdbc.ConnectionFactory;
import com.study.dao.jdbc.ItemPedidoDao;
import com.study.dao.jdbc.PedidoDao;
import com.study.dao.jdbc.ProdutoDao;
import com.study.model.ItemPedido;
import com.study.model.ItemPedidoPK;

@Path("/pedido/item")
public class ItemPedidoResource {
	
	@GET
	@Path("{idPedido}/{idProduto}")
	@Produces(MediaType.APPLICATION_JSON)
	public ItemPedido get(@PathParam("idPedido") int idPedido,
			              @PathParam("idProduto") String idProduto){
		try {
			Connection conn = new ConnectionFactory().getConnection();
			IItemPedidoDao dao = new ItemPedidoDao(conn);
			IPedidoDao pedido = new PedidoDao(conn);
			IProdutoDao produto = new ProdutoDao(conn);
			ItemPedidoPK pk = new ItemPedidoPK(pedido.get(idPedido), produto.get(idProduto));
			return dao.get(pk);
		} catch (Exception e) {
			return null;
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String post(ItemPedido itemPedido){
		try {
			Connection conn = new ConnectionFactory().getConnection();
			IItemPedidoDao dao = new ItemPedidoDao(conn);
			dao.insert(itemPedido);
			return "{\"msg\": \"Item inserido com sucesso!\"}";
		} catch (Exception e) {
			return "{\"msg\": \"Erro ao inserir o item!\"}";
		}
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String put(ItemPedido itemPedido){
		try {
			Connection conn = new ConnectionFactory().getConnection();
			IItemPedidoDao dao = new ItemPedidoDao(conn);
			dao.update(itemPedido);
			return "{\"msg\": \"Item atualizado com sucesso!\"}";
		} catch (Exception e) {
			return "{\"msg\": \"Erro ao atualizar o item!\"}";
		}
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String delete(ItemPedido itemPedido){
		try {
			Connection conn = new ConnectionFactory().getConnection();
			IItemPedidoDao dao = new ItemPedidoDao(conn);
			ItemPedidoPK pk = new ItemPedidoPK(itemPedido.getPedido(), 
					                           itemPedido.getProduto());
			dao.delete(pk);
			return "{\"msg\": \"Item deletado com sucesso!\"}";
		} catch (Exception e) {
			return "{\"msg\": \"Erro ao deletar o item!\"}";
		}
	}
	
	@GET
	@Path("/all/{idPedido}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ItemPedido> list(@PathParam("idPedido") int idPedido){
		try {
			Connection conn = new ConnectionFactory().getConnection();
			IItemPedidoDao dao = new ItemPedidoDao(conn);
			return dao.findByPedido(new PedidoDao(conn).get(idPedido));
		} catch (Exception e) {
			return null;
		}
	}
	
}
