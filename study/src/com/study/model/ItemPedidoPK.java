package com.study.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ItemPedidoPK {
	private Produto produto;
	private Pedido pedido;
	
	public ItemPedidoPK(Pedido pedido, Produto produto) {
		this.pedido = pedido;
		this.produto = produto;
	}
	
	public Pedido getPedido() {
		return pedido;
	}
	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	
}
