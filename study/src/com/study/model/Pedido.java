package com.study.model;

import java.io.Serializable;
import java.sql.*;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Pedido implements Serializable {
	private static final long serialVersionUID = 622809024321952146L;
	private Integer numeroPedido;
	private Date dataPedido;
	private Cliente cliente; 
	private List<ItemPedido> itemPedidoList;
	
	public List<ItemPedido> getItemPedidoList() {
		return itemPedidoList;
	}
	public void setItemPedidoList(List<ItemPedido> itemPedidoList) {
		this.itemPedidoList = itemPedidoList;
	}
	public Integer getNumeroPedido() {
		return numeroPedido;
	}
	public void setNumeroPedido(Integer numeroPedido) {
		this.numeroPedido = numeroPedido;
	}
	public Date getDataPedido() {
		return dataPedido;
	}
	public void setDataPedido(Date dataPedido) {
		this.dataPedido = dataPedido;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	
}
