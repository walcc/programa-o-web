package com.study.app;

import java.sql.Connection;
import com.study.dao.jdbc.ConnectionFactory;

public class ConnectionFactoryTest {
	public static void main(String[] args) {
		try {
			Connection conn = new ConnectionFactory().getConnection();
			System.out.println("Conectado");
			conn.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
