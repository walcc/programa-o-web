package com.study.app;

import java.sql.Connection;
import com.study.dao.IProdutoDao;
import com.study.dao.jdbc.ConnectionFactory;
import com.study.dao.jdbc.ProdutoDao;
import com.study.model.Produto;

public class ProdutoDaoInsertTeste {
	public static void main(String[] args) {
		try {
			Connection conn = new ConnectionFactory().getConnection();
			System.out.println("Conectado");
			
			IProdutoDao dao = new ProdutoDao(conn);
			
			Produto produto = new Produto();
			produto.setCodigo("000-2");
			produto.setNome("Borracha");
			
			dao.insert(produto);
			
			conn.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
