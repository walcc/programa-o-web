package com.study.app;

import java.sql.Connection;
import java.util.List;
import com.study.dao.IPedidoDao;
import com.study.dao.jdbc.ConnectionFactory;
import com.study.dao.jdbc.PedidoDao;
import com.study.model.Pedido;

public class PedidoDaoSelectAllTeste {
	public static void main(String[] args) {
		try {
			Connection conn = new ConnectionFactory().getConnection();
			System.out.println("Conectado");
			
			IPedidoDao dao = new PedidoDao(conn);
			
			List<Pedido> list = dao.listAll();
			for(int i =0; i < list.size(); i++){
				System.out.println(list.get(i).getNumeroPedido() + " " +
			                       list.get(i).getCliente().getNome() + " " +
						           list.get(i).getDataPedido());
			}
			
			conn.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
