package com.study.app;

import java.sql.Connection;

import com.study.dao.IClienteDao;
import com.study.dao.jdbc.ClienteDao;
import com.study.dao.jdbc.ConnectionFactory;
import com.study.model.Cliente;

public class ClienteDaoInsertTeste {
	public static void main(String[] args) {
		try {
			Connection conn = new ConnectionFactory().getConnection();
			System.out.println("Conectado");
			
			IClienteDao dao = new ClienteDao(conn);
			
			Cliente cliente = new Cliente();
			cliente.setNome("Walfrido");
			cliente.setInscricaoFederal("135.1235");
			
			dao.insert(cliente);
			
			conn.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
