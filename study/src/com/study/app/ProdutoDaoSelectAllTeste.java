package com.study.app;

import java.sql.Connection;
import java.util.List;
import com.study.dao.IProdutoDao;
import com.study.dao.jdbc.ConnectionFactory;
import com.study.dao.jdbc.ProdutoDao;
import com.study.model.Produto;

public class ProdutoDaoSelectAllTeste {
	public static void main(String[] args) {
		try {
			Connection conn = new ConnectionFactory().getConnection();
			System.out.println("Conectado");
			
			IProdutoDao dao = new ProdutoDao(conn);
			
			List<Produto> list = dao.listAll();
			for(int i =0; i < list.size(); i++){
				System.out.println(list.get(i).getNome());
			}
			
			conn.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
