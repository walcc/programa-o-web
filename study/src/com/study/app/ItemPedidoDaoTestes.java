package com.study.app;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.NumberFormat;
import java.util.List;
import com.study.dao.IItemPedidoDao;
import com.study.dao.jdbc.ConnectionFactory;
import com.study.dao.jdbc.ItemPedidoDao;
import com.study.model.ItemPedido;
import com.study.model.Pedido;
import com.study.model.Produto;

public class ItemPedidoDaoTestes {
	public static void main(String[] args) {
		try {
			Connection conn = new ConnectionFactory().getConnection();
			IItemPedidoDao dao = new ItemPedidoDao(conn);
			
			// Referencia aos objetos da PK
			Pedido pedido = new Pedido();
			pedido.setNumeroPedido(1);
			
			Produto produto = new Produto();
			produto.setCodigo("000-2");
			
			// Id = PK			
			List<ItemPedido> list =  dao.listAll();//new ArrayList<ItemPedidoPK>(); //new ItemPedidoPK(pedido, produto);
			for(int i = 0; i < list.size(); i++){
				ItemPedido itemPedido = list.get(i);
				System.out.print("N�mero Pedido:" + itemPedido.getPedido().getNumeroPedido());
				System.out.print(" | Produto:" + itemPedido.getProduto().getNome());
				System.out.print(" | Quantidade:" + itemPedido.getQuantidade());
				System.out.print(" | Valor:" + NumberFormat.getCurrencyInstance().format(itemPedido.getValorUnitario()));
				BigDecimal valorTotal = itemPedido.getValorUnitario().multiply(itemPedido.getQuantidade());
				System.out.print(" | Total R$:" + valorTotal);
				System.out.println("");
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
