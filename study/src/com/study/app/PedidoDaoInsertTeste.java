package com.study.app;

import java.sql.Connection;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.study.dao.IClienteDao;
import com.study.dao.IPedidoDao;
import com.study.dao.jdbc.ClienteDao;
import com.study.dao.jdbc.ConnectionFactory;
import com.study.dao.jdbc.PedidoDao;
import com.study.model.Cliente;
import com.study.model.Pedido;

public class PedidoDaoInsertTeste {
	public static void main(String[] args) {
		try {
			Connection conn = new ConnectionFactory().getConnection();
			System.out.println("Conectado");
			
			IPedidoDao dao = new PedidoDao(conn);
			IClienteDao daoCliente = new ClienteDao(conn);
			
			
			Pedido pedido = new Pedido();
			Cliente cliente = daoCliente.get(2);
			String dataString = "2016-10-22";
			DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
			Date data = new Date(fmt.parse(dataString).getTime());
			pedido.setDataPedido(data);
			pedido.setCliente(cliente);
			
			dao.insert(pedido);
			
			conn.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
