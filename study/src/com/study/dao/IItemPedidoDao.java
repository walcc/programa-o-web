package com.study.dao;

import java.util.List;
import com.study.model.ItemPedido;
import com.study.model.Pedido;

public interface IItemPedidoDao extends IDao<ItemPedido>{
	List<ItemPedido> findByPedido(Pedido pedido) throws Exception;
}
