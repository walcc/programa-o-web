package com.study.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.study.dao.IClienteDao;
import com.study.model.Cliente;

public class ClienteDao extends AbstractDao implements IClienteDao {

	public ClienteDao(Connection conn) {
		super(conn);
	}

	public void insert(Cliente model) throws Exception {
		final String SQL = "insert into clientes (nome, inscricao_federal) " +
	                       "values (?, ?)";
		PreparedStatement ps = conn.prepareStatement(SQL);
		ps.setString(1, model.getNome());
		ps.setString(2, model.getInscricaoFederal());
		ps.execute();
		ps.close();
	}

	public void update(Cliente model) throws Exception {
		final String SQL = "update clientes set " +
	                       "nome=?, inscricao_federal=? " +
				           "where codigo=?";
		PreparedStatement ps = conn.prepareStatement(SQL);
		ps.setString(1, model.getNome());
		ps.setString(2, model.getInscricaoFederal());
		ps.setInt(3, model.getCodigo());
		ps.execute();
		ps.close();
	}

	public void delete(Object id) throws Exception {
		final String SQL = "delete from clientes where codigo=?";
		PreparedStatement ps = conn.prepareStatement(SQL);
		ps.setInt(1, (Integer) id);
		ps.execute();
		ps.close();
	}

	public List<Cliente> listAll() throws Exception {
		final String SQL = "select * from clientes";
		List<Cliente> result = new ArrayList<Cliente>();
		PreparedStatement ps = conn.prepareStatement(SQL);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			result.add(buildModel(rs));
		}
		return result;
	}

	public Cliente get(Object id) throws Exception {
		final String SQL = "select * from clientes " +
	                       "where codigo=?";
		Cliente result = null;
		PreparedStatement ps = conn.prepareStatement(SQL);
		ps.setInt(1, (Integer) id);
		ResultSet rs = ps.executeQuery();
		if (rs.next()){
			result = buildModel(rs);
		}
		rs.close();
		ps.close();
		return result;
	}

	public List<Cliente> findByNome(String nome) throws Exception {
		final String SQL = "select * from clientes " +
	                       "where nome like ?";
		List<Cliente> result = new ArrayList<Cliente>();
		PreparedStatement ps = conn.prepareStatement(SQL);
		ps.setString(1, nome);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			result.add(buildModel(rs));
		}
		return result;
	}
	
	private Cliente buildModel(ResultSet rs) throws Exception{
		Cliente result = new Cliente();
		result.setCodigo(rs.getInt("codigo"));
		result.setNome(rs.getString("nome"));
		result.setInscricaoFederal(rs.getString("inscricao_federal"));
		return result;
	}

}
