package com.study.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.study.dao.IItemPedidoDao;
import com.study.model.ItemPedido;
import com.study.model.ItemPedidoPK;
import com.study.model.Pedido;
import com.study.model.Produto;

public class ItemPedidoDao extends AbstractDao implements IItemPedidoDao{

	public ItemPedidoDao(Connection conn) {
		super(conn);
	}

	public void insert(ItemPedido model) throws Exception {
		final String SQL = "insert into itens_pedidos "
				         + "(numero_pedido,codigo_produto,"
				         + "quantidade,valor_unitario,valor_total) "
				         + "values(?,?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(SQL);
		ps.setInt(1, model.getPedido().getNumeroPedido());
		ps.setString(2, model.getProduto().getCodigo());
		ps.setBigDecimal(3, model.getQuantidade());
		ps.setBigDecimal(4, model.getValorUnitario());
		ps.setBigDecimal(5, model.getvalorTotal());
		ps.execute();
		ps.close();
	}

	public void update(ItemPedido model) throws Exception {
		final String SQL = "update intens_pedidos set "
				         + "quantidade=?,valor_unitario?,valor_total=? "
				         + "where numero_pedido? and codigo_produto=?";
		PreparedStatement ps = conn.prepareStatement(SQL);
		ps.setBigDecimal(1, model.getQuantidade());
		ps.setBigDecimal(2, model.getValorUnitario());
		ps.setBigDecimal(3, model.getvalorTotal());
		ps.setInt(4, model.getPedido().getNumeroPedido());
		ps.setString(5, model.getProduto().getCodigo());
		ps.execute();
		ps.close();
	}

	public void delete(Object id) throws Exception {
		ItemPedidoPK pk = (ItemPedidoPK) id;
		final String SQL = "delete from itens_pedidos where "
                         + "where codigo_produto=? and numero_pedido=?";
		PreparedStatement ps = conn.prepareStatement(SQL);
		ps.setString(1, pk.getProduto().getCodigo());
		ps.setInt(2, pk.getPedido().getNumeroPedido());
		ps.execute();
		ps.close();
	}

	public List<ItemPedido> listAll() throws Exception {
		final String SQL = "select * from itens_pedidos";
		List<ItemPedido> result = new ArrayList<ItemPedido>();
		PreparedStatement ps = conn.prepareStatement(SQL);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			result.add(buildModel(rs,null));
		}
		return result;
	}
	
	public List<ItemPedido> findByPedido(Pedido pedido) throws Exception {
		final String SQL = "select * from itens_pedidos where numero_pedido=?";
		List<ItemPedido> result = new ArrayList<ItemPedido>();
		PreparedStatement ps = conn.prepareStatement(SQL);
		ps.setInt(1, pedido.getNumeroPedido());
		ResultSet rs = ps.executeQuery();
		ItemPedidoPK pk = new ItemPedidoPK(pedido, null);
		while(rs.next()){
			result.add(buildModel(rs,pk));
		}
		return result;
	}

	public ItemPedido get(Object id) throws Exception {
		ItemPedido result = null;
		ItemPedidoPK pk = (ItemPedidoPK) id;
		final String SQL = "select * from itens_pedidos " +
				           "where codigo_produto=? and numero_pedido=?";
		PreparedStatement ps = conn.prepareStatement(SQL);
		ps.setString(1, pk.getProduto().getCodigo());
		ps.setInt(2, pk.getPedido().getNumeroPedido());
		ResultSet rs = ps.executeQuery();
		if(rs.next()){
			result = buildModel(rs,pk);
		}
		return result;
	}
	
	private ItemPedido buildModel(ResultSet rs, ItemPedidoPK pk) throws Exception {
		try {
			Pedido pedido = pk.getPedido();
			Produto produto = pk.getProduto();
			ItemPedido result = new ItemPedido(pedido,produto);
			result.setQuantidade(rs.getBigDecimal("quantidade"));
			result.setValorUnitario(rs.getBigDecimal("valor_unitario"));
			result.setValorTotal(rs.getBigDecimal("valor_total"));
			return result;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

}
