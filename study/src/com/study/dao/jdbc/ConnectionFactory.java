package com.study.dao.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionFactory {
	
	public Connection getConnection() throws Exception{
		Class.forName("com.mysql.jdbc.Driver");   
		String user = "root";
		String pwd = "sysdba";
		String dbName = "lojadb";
		String dbHostName = "localhost";
		String url = "jdbc:mysql://" + dbHostName + "/" + dbName;
		Connection conn = DriverManager.getConnection(url,user,pwd);
		return conn;
	}
}
